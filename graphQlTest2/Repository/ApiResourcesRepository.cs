using System.Collections.Generic;
using System.Threading.Tasks;
using graphQlTest2.Data;
using graphQlTest2.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace graphQlTest2.Repository
{
    public class ApiResourcesRepository
    {
        private readonly GraphQLDbContext _context;

        public ApiResourcesRepository(GraphQLDbContext context)
        {
            _context = context;
        }

        public async Task<ApiResources> GetOneApiResource(int id)
        {
            return await _context.ApiResources.SingleOrDefaultAsync(a => a.Id == id);
        }
        public async Task<List<ApiResources>> GetAllApiResources()
        {
            return await _context.ApiResources.ToListAsync();
        }
        public async Task<ApiResources> CreateApiResourceAsync(ApiResources apiResources)
        {
            await _context.ApiResources.AddAsync(apiResources);
            await _context.SaveChangesAsync();
            return apiResources;
        }

        public async Task<ApiResources> UpdateApiResourceAsync(ApiResources apiResources)
        {
            var updateApiResource=_context.ApiResources.Update(apiResources);
            await _context.SaveChangesAsync();
            return updateApiResource.Entity;
        }

        public async Task<ApiResources> DeleteApiResource(int id)
        {
            var apiResource=await _context.ApiResources.SingleOrDefaultAsync(a => a.Id == id);
            _context.ApiResources.Remove(apiResource);
            await _context.SaveChangesAsync();
            
            return apiResource;

        }
        
    }
}