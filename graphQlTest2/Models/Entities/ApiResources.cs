namespace graphQlTest2.Models.Entities
{
    public class ApiResources
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
    }
}