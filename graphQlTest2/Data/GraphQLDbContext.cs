using graphQlTest2.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace graphQlTest2.Data
{
    public class GraphQLDbContext :DbContext
    {
        public GraphQLDbContext(DbContextOptions<GraphQLDbContext> options) : base(options)
        {
        }
        
            
        public DbSet<ApiResources> ApiResources { get; set; }
    }
}