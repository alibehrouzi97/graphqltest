using GraphQL.Types;
using graphQlTest2.Models.Entities;

namespace graphQlTest2.GraphQl.Types
{
    public class ApiResourceType : ObjectGraphType<ApiResources>
    {
        public ApiResourceType()
        {
            Field(a => a.Id);
            Field(a => a.Description);
            Field(a => a.Name);
            Field(a => a.DisplayName);
            
        }
    }
}