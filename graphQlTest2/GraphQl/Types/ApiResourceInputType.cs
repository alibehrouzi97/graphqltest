using GraphQL.Types;

namespace graphQlTest2.GraphQl.Types
{
    public class ApiResourceInputType : InputObjectGraphType
    {
        public ApiResourceInputType()
        {
            Name = "apiResourceInput";
            Field<IdGraphType>("id");
            Field<StringGraphType>("name");
            Field<StringGraphType>("description");
            Field<StringGraphType>("displayName");
        }
    }
}