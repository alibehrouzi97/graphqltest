using GraphQL;
using GraphQL.Types;

namespace graphQlTest2.GraphQl
{
    public class GraphQlTestSchema: Schema
    {
        public GraphQlTestSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<GraphQlTestQuery>();
            Mutation = resolver.Resolve<GraphQlTestMutation>();

        }
    }
}