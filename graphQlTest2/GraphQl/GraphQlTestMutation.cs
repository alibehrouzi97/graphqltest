using GraphQL.Types;
using graphQlTest2.GraphQl.Types;
using graphQlTest2.Models.Entities;
using graphQlTest2.Repository;

namespace graphQlTest2.GraphQl
{
    public class GraphQlTestMutation : ObjectGraphType
    {
        public GraphQlTestMutation(ApiResourcesRepository apiResourcesRepository)
        {
            FieldAsync<ApiResourceType>(
                "createApiResource",
                arguments: new QueryArguments(
                    new  QueryArgument<NonNullGraphType<ApiResourceInputType>> {Name = "apiResource"}),
                resolve: async context =>
                {
                    var apiResource = context.GetArgument<ApiResources>("apiResource");
                    return await context.TryAsyncResolve(async c =>
                        await apiResourcesRepository.CreateApiResourceAsync(apiResource));
                });

            FieldAsync<ApiResourceType>(
                "updateApiResource",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<ApiResourceInputType>>
                    {Name = "updateApiResource"}),
                resolve: async context =>
                {
                    var updateApiResource = context.GetArgument<ApiResources>("updateApiResource");
                    return await context.TryAsyncResolve(async c =>
                        await apiResourcesRepository.UpdateApiResourceAsync(updateApiResource));
                }
            );
            FieldAsync<ApiResourceType>(
                "deleteApiResource",
                arguments:new QueryArguments( 
                    new QueryArgument<NonNullGraphType<IdGraphType>> {Name = "id"}
                    ),
                    resolve: async context =>
                    {
                        var id = context.GetArgument<int>("id");
                        return await context.TryAsyncResolve(async c =>
                            await apiResourcesRepository.DeleteApiResource(id));
                    } 
                );


        }
    }
}