using GraphQL.Types;
using graphQlTest2.GraphQl.Types;
using graphQlTest2.Repository;

namespace graphQlTest2.GraphQl
{
    public class GraphQlTestQuery: ObjectGraphType
    {
        public GraphQlTestQuery(ApiResourcesRepository apiResourceRepository)
        {
            
            Field<ListGraphType<ApiResourceType>>(
                "apiResources",
                resolve: context => apiResourceRepository.GetAllApiResources()
            );

            Field<ApiResourceType>(
                "apiResource",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> {Name = "id"}
                ),
                resolve: context =>
                {
                    var id = context.GetArgument<int>("id");
                    return apiResourceRepository.GetOneApiResource(id);

                }
            );
            


        }
    }
}